#include <Keypad.h>
#include "lorax.h"
lorax trasm;
#include "heltec.h"
#include "time.h"
#include <driver/adc.h>
#define lock 36
#define NUM_RETRY_RADIO 3
int retry_tx_radio =0;
#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  1800        /* Time ESP32 will go to sleep (in seconds) */
#define versione 2 //scrivere versione heltec per lettura batteria 1 o 2 // se versione scheda nuova 37 vecchia 13
#define BAND    868E6  //you can set band here directly,e.g. 868E6,915E6
int val;
bool ok;
uint16_t serial_num = 1;
uint16_t radice = 7000;
double v_batt;
////////////////////////////////////////////////////////////////////
#define Password_Length 8
bool pwd = false;
int signalPin = 12;
int ledVerde = 21;
char Data[Password_Length]; 
char Master[Password_Length] = "456456B"; 
byte data_count = 0, master_count = 0;
bool Pass_is_good;
char customKey;
const int wdtTimeout = 60000; 
hw_timer_t *timer = NULL;

//---------------------------watchdog initialize----------------------------------
void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  esp_restart();
}
const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {15, 2, 32, 4}; 
byte colPins[COLS] = {6, 7, 33, 11}; 

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
/////////////////////////////////////////////////////////////////////

RTC_DATA_ATTR int bootCount = 0;

bool trigger=false;


void setup(){
  readbat();
  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);
  //WIFI Kit series V1 not support Vext control
  Heltec.begin(false /*DisplayEnable Enable*/, true /*LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  digitalWrite(ledVerde, HIGH);
  pinMode(lock, INPUT);
  val = digitalRead(lock);
  Serial.println(val); 
  //Increment boot number and print it every reboot
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));
  trigger=false;
  delay(2000);
  if (esp_sleep_get_wakeup_cause()==1||esp_sleep_get_wakeup_cause()==2||esp_sleep_get_wakeup_cause()==3)
  {
    trigger = true;
  }
   if(val == 0)
  {
    allarmeon();
    checkallarme();
  }
  
  val = digitalRead(lock);
  

  if(val == 0)
  {
    Serial.println("parametro 1 ");
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_36, 1);
  }
  else
  {
    aggiornaremoto();
    Serial.println("parametro 0 ");
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_36, 0);
  }
   esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
 LoRa.end();
  LoRa.sleep();
  alloff();
  delay(100);
  pinMode(5,INPUT);
   pinMode(14,INPUT);
   pinMode(15,INPUT);
   pinMode(16,INPUT);
   pinMode(17,INPUT);
   pinMode(18,INPUT);
   pinMode(25,INPUT);
  pinMode(19,INPUT);
  pinMode(26,INPUT);
  pinMode(27,INPUT);
  delay(100);
  Serial.println("Going to sleep now");
  delay(2);
  timerAlarmDisable(timer);
  timerDetachInterrupt(timer);
  timerEnd(timer);
  timer = NULL;
  esp_deep_sleep_start();
  
  Serial.println("This will never be printed");

}

void loop(){
  //This is not going to be called
}

void aggiornaremoto()
{
      String message = "";
      if(!val)
      {
        message="1";
        message+=",";
      if(ok)
      {
      message += "1";
      Serial.print("invio ingresso autorizzato");
      }
      else {message+="0";Serial.print("invio ingresso non autorizzato");}
      }
      else
      {
         message="0";
      }
      
      
     delay(100);
      //aspetto la risposta
      bool risp = false;
      retry_tx_radio =0;
        v_batt = readbat();
      while(!risp && retry_tx_radio<NUM_RETRY_RADIO)
  {
           timerWrite(timer, 0);
           retry_tx_radio++;
           Serial.print("Invio tentativo: ");
           Serial.println(retry_tx_radio);
          trasm.lorasend(radice, serial_num, v_batt, message);
         delay(100);
        risp = trasm.checkanswer();
         }
       if (risp)
       {
        Serial.print("master aggiornato");
        ok = true;
         alloff();
         controllok();
       }

}

void checkallarme()
{ 
  Serial.println("controllo allarme");
  if(ok == false)
  {
    Serial.println("accendo allarme");
    //accendi pin allarme
    if(trigger)
    {
    readpwd();
    }
    else {
     ok=true;
    }
    aggiornaremoto();
    while(ok == false && val == 0)
    {
      
      
      aggiornaremoto();
      //da fare anche verifica codice
      val = digitalRead(lock);
    }
    Serial.println("spengo allarme");
    digitalWrite(ledVerde, LOW);
    
  }
}

void readpwd(){
  int i = 0;
 double timeStart = clock();
 int tent = 0;
 while(tent<3 && pwd == false)
 {
  tent++;
  Serial.print("tentativo: ");
  Serial.println(tent);
   i=0;
  while(i<=Password_Length-2)
  {
    if ((clock() - timeStart) / CLOCKS_PER_SEC >= 35)
  {
  Serial.println("tempo scaduto");// time in seconds
        break;
  }
    char key = keypad.getKey();
  if(key)
  {
    digitalWrite(25, HIGH);
    delay(100);
    digitalWrite(25, LOW);
  if (key == Master[i] ){
    Serial.println(key);
    i++;
    pwd = true;
  }
  else if(key == 'C')
  { 
    i=0;
    pwd = false;
  }
  else
  {
    Serial.println(key);
    pwd = false;
    i++;
  }
  }
 
  }
   if ((clock() - timeStart) / CLOCKS_PER_SEC >= 40)
  {
  Serial.println("tempo scaduto");// time in seconds
        break;
  }
  if(pwd==false)
  {
    digitalWrite(25, HIGH);
    delay(100);
    digitalWrite(25, LOW);
  }
 }
  if(pwd == true && i==Password_Length-1)
  {Serial.println("pwd essatta");
  ok=true;
  digitalWrite(ledVerde, LOW);
  controllok();
  }
  else pwd = false;

}

void allarmeon()
{
  Serial.print("led rosso on");
}
void controllok()
{
  Serial.print("led verde on");
}
void alloff()
{
  digitalWrite(ledVerde, HIGH);  
  Serial.print("led off");
}

double readbat(){
  pinMode(21, OUTPUT);
  digitalWrite(21, LOW);
  delay(1000);
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_0,ADC_ATTEN_DB_6);
      delay(400);
   if(versione == 2)
   {
    double battery = adc1_get_raw(ADC1_CHANNEL_1); // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
    Serial.print("Heltec versione 2.1 pin 37:");
  Serial.println(battery);
  battery = battery * 0.00307;
  return battery;//costante calcolata con media misure
   }
   else if (versione == 1)
   {
     double reading = analogRead(13); // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
     if(reading < 1 || reading >= 4095)
      Serial.print("Heltec versione 1 pin 13:");
     Serial.println(reading);
     reading = reading  * 0.00182;
      return reading;//costante calcolata con media misure
   }
   return 1;
}
