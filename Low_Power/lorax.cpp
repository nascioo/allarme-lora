#include <Wire.h>
#include "lorax.h"
#include <StringSplitter.h>
#include <Preferences.h>
Preferences preferences;
#define DELAY_RISP_TX 5
unsigned long milli_s_prec;
unsigned long milli_s;
uint8_t time_read_data;
uint8_t num_pack = 0;
int rssi_rx = 0;

uint8_t delay_risp_tx = 0;


void lorax::lorasend(uint16_t radice, uint16_t serial_num, double v_batt, String pkt)
{ 
  preferences.begin("radio_tx_data",false);
  rssi_rx = preferences.getUInt("rssi",0);
  num_pack = preferences.getUInt("num_pac",0);
  preferences.end();  
  String packet_tx_string = (String(serial_num+radice) + ','+ String(num_pack) + ','+ String(v_batt,3)+",0,"+ pkt +","+ String(rssi_rx));
  Serial.print("Sending packet: " + packet_tx_string);
  // send packet
  LoRa.beginPacket();
  LoRa.setTxPower(14,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.print(packet_tx_string);

  LoRa.endPacket();

  Serial.println("OK!");
}


//**********************************************************************************************
//verifico risposta affermativa ricevitore
bool lorax::read_lora_pac(int packetsize){
  if (packetsize == 0){
    Serial.println("No Risposta");
    delay(200);
    return false;          // if there's no packet, return
  }
  // read packet header bytes:
  String memos = "";
  while (LoRa.available())
  {
    memos += (char)LoRa.read();
  }
  memos+= '\0';
  Serial.println("Message: " + memos);
  rssi_rx = Heltec.LoRa.packetRssi();
  Serial.println("RSSI: " + String(rssi_rx));
  Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println();
  StringSplitter *splitter = new StringSplitter(memos, ',', 30);
  Serial.println(splitter->getItemAtIndex(0));
  if(splitter->getItemAtIndex(0) == "OK"){
    Serial.println("ok ricevuto esci");
    //salva parametri per prossima riaccensione
    preferences.begin("radio_tx_data",false);
    preferences.putUInt("rssi",rssi_rx);
    num_pack++;
    preferences.putUInt("num_pac",num_pack);
    preferences.end();  
  }
  else{
    Serial.println("Risp Errata");

  }
  return true;
}
//**********************************************************************************************


void lorax::orologio(void){
  milli_s = abs(millis() - milli_s_prec);
  if(milli_s>999){ //secondi
    milli_s_prec = millis();
    milli_s = 0;
    time_read_data++;
    delay_risp_tx++;
  }
  return;
}

bool lorax::checkanswer(){
 delay_risp_tx = 0;
    while(delay_risp_tx < DELAY_RISP_TX)
    {
      delay(800);
      if(read_lora_pac(Heltec.LoRa.parsePacket()))
      {
        delay_risp_tx = DELAY_RISP_TX;
        Serial.println("pacchetto ricevuto da CR");
        return true;
      }
      delay_risp_tx++;
    }
      digitalWrite(LED,!digitalRead(LED));
      Serial.println("ricevuto nulla");
      return false;
      orologio();  
    
}
