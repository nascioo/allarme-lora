#include <Wire.h>
#include <heltec.h>
#include "lorax.h"
#include <StringSplitter.h>
#include <Preferences.h>
Preferences preferences;
//pin dichiarati per evitare problemi con MicroSD
#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
SPIClass loraSPI(VSPI);
String rssi = "RSSI --";
String packSize = "--";
String packet ;
#define NUM_RETRY_RADIO 3
#define DELAY_RISP_TX 5
unsigned long milli_s_prec;
unsigned long milli_s;
uint8_t time_read_data;
uint8_t num_pack = 0;
int rssi_rx = 0;
uint8_t retry_tx_radio = 0;
uint8_t delay_risp_tx = 0;
int count_crash = 0;
String mex_send, mex_rx;
#define INTERVALLO_CAMPIONI 900
#define LORA_CRASH INTERVALLO_CAMPIONI+100




void lorax::lorasend(uint16_t radice, uint16_t serial_num, double v_batt, String pkt)
{ 
   LoRa.setTxPower(20,PA_OUTPUT_PA_BOOST_PIN);
  preferences.begin("radio_tx_data",false);
  rssi_rx = preferences.getUInt("rssi",0);
  num_pack = preferences.getUInt("num_pac",0);
  preferences.end();  
  String packet_tx_string = (String(serial_num+radice) + ','+ String(num_pack) + ','+ String(v_batt,3)+",0,"+ pkt + String(rssi_rx));
  Serial.print("Sending packet: " + packet_tx_string);
  // send packet
  LoRa.beginPacket();
  LoRa.print(packet_tx_string);

  LoRa.endPacket();
  Serial.println("OK!");
}

//---------------------------FUNZIONI UTILIZZATE SEMPRE DA GEOMILLA 6------------------------------
void lorax::tx_pack(String memo_mex){
  //composizione con intestazione pacchetto
  String packet_tx_string =  memo_mex;
 LoRa.beginPacket();                   // start packet
 LoRa.print(packet_tx_string);                 // add payload
 LoRa.endPacket();                     // finish packet and send it  
  Serial.println("invio dati lora");
  Serial.println(packet_tx_string);
  //ritentativi trasmissione
  return;
}


void lorax::orologio(void){
  milli_s = abs(millis() - milli_s_prec);
  if(milli_s>999){ //secondi
    milli_s_prec = millis();
    milli_s = 0;
    time_read_data++;
    delay_risp_tx++;
  }
  return;
}




String lorax::checkanswer(){
LoRa.setTxPower(20,PA_OUTPUT_PA_BOOST_PIN);
 delay_risp_tx = 0;
      int packetsize = 0;
      packetsize = LoRa.parsePacket();
      packet = read_lora_pac(packetsize);
      orologio();  
      return packet;
}


//ricevitore**************************************************************
String lorax::read_lora_pac(int packetsize){
  String packet ="" ;
  if (packetsize == 0){
    return packet;          // if there's no packet, return
  }
  // read packet header bytes:
  String memos = "";
  while (LoRa.available())
  {
    memos += (char)LoRa.read();
  }
  memos+= '\0';
  memos = memos.substring(0);
  Serial.println("Message: " + memos);
  Serial.println("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println();
  StringSplitter *splitter = new StringSplitter(memos, ',', 30);
 // String risp_tx = "OK," + String(INTERVALLO_CAMPIONI) + ",S";
  String risp_tx = "OK";
  //estrazione componenti stringa per poi girare via seriale al datalogger
  int itemCount = splitter->getItemCount();
  String memo_serial_pack = "";
  for(int i=0;i<itemCount;i++){  //così taglio via ultimo 0 connesso con header pacchetto terminale di 2 caratteri
    memo_serial_pack += splitter->getItemAtIndex(i) + ',';
  }
  //così ultimo aggiunge terminatore che è /n solito
  memo_serial_pack += splitter->getItemAtIndex(itemCount) + String(LoRa.packetRssi()) + '\n';
   Serial.println("packet:");
  Serial.println(memo_serial_pack);
  delay(1000);
  
  count_crash = 0;
  packet = memo_serial_pack;
  return packet;
}


String lorax::getserial(int pos, String pp){
 StringSplitter *splitter = new StringSplitter(pp, ',', 30);
pp = "";
pp = splitter->getItemAtIndex(pos);
return pp;
}

String lorax::getseriale(String pp){
 StringSplitter *splitter = new StringSplitter(pp, ',', 30);
pp = "";
pp += splitter->getItemAtIndex(0);
return pp;
}
