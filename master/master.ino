#include <UniversalTelegramBot.h>
#include <ArduinoJson.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <StringSplitter.h>
#include <OneWire.h>
#include "lorax.h"
#include "time.h"
#include <HTTPClient.h>
lorax lora;
#include "heltec.h"
#define lock 4
#define BAND    868E6  //you can set band here directly,e.g. 868E6,915E6
#define versione 1 //scrivere versione heltec per lettura batteria 1 o 2 // se versione scheda nuova 37 vecchia 13
bool connection = false;
double timeStart;
double timeStart1;
String server = "http://economialocus.altervista.org/upload/upload.php";
int val;
bool ok;
bool alarmt;
bool tele = true;
bool cancello = false; //true aperto false chiuso
String statocancello;
bool autorizzazione = false;
bool aggiornamento = true;
String message;
String botaggiornamento; //messaggio da mandare sul bot
int packetsize =0;
uint16_t serial_num = 0;
uint16_t radice = 7000;
// Wifi network station credentials
#define WIFI_SSID "TP-LINK_Campagna"
#define WIFI_PASSWORD "milasotino"

// Telegram BOT Token (Get from Botfather)
#define BOT_TOKEN "1697702115:AAFzPbK5E2DYdMx7wt3SugH9YDdqC4RlXn8"
#define CHAT_ID "109032568"
#define CHAT_ID2 "347374012" 
String idaut = "";
const unsigned long BOT_MTBS = 1000; // mean time between scan messages

WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOT_TOKEN, secured_client);
unsigned long bot_lasttime;          // last time messages' scan has been done
bool Start = false;


RTC_DATA_ATTR int bootCount = 0;


const int wdtTimeout = 30000; 
hw_timer_t *timer = NULL;
//---------------------------watchdog initialize----------------------------------
void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  esp_restart();
}




void setup(){
  //WIFI Kit series V1 not support Vext control
  Heltec.begin(false /*DisplayEnable Enable*/, true /*LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
    timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);                          //enable interrupt
   Serial.print("Connecting to Wifi SSID ");
  Serial.print(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  secured_client.setCACert(TELEGRAM_CERTIFICATE_ROOT); // Add root certificate for api.telegram.org
  
  delay(900);
  // attempt to connect to Wifi network:
  timerWrite(timer, 0);
  if (WiFi.status() != WL_CONNECTED)
  {
    connection = false;
  }
  else connection = true;

  if (connection == true)
  {
  Serial.print("\nWiFi connected. IP address: ");
  Serial.println(WiFi.localIP());
   bot.sendMessage(CHAT_ID, "Allarme attivo", "");
  Serial.print("Retrieving time: ");
  configTime(0, 0, "pool.ntp.org"); // get UTC time via NTP
  time_t now = time(nullptr);
  while (now < 24 * 3600)
  {
    timerWrite(timer, 0);
    Serial.print(".");
    delay(100);
    now = time(nullptr);
    Serial.println(now);
  }
  }
}

void checkinternet()
{
  if (WiFi.status() != WL_CONNECTED) {
      connection = false;
      WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
      delay(3000);
      if (WiFi.status() != WL_CONNECTED) {
      delay(500);
      connection = false;
      }
      else
      {
       connection = true;
       Serial.println("WiFi connected");
        Serial.println("IP address: ");
       Serial.println(WiFi.localIP());
       }
  }else
  {
    connection = true;
  }
}

void loop(){
  timerWrite(timer, 0);
checkinternet();
  checklora();
  timerWrite(timer, 0);
  if(connection == true)
  {
    if ( botaggiornamento.length()>2 && tele == true)
    {
      timeStart1 = clock();
     bot.sendMessage(CHAT_ID, botaggiornamento, "");
     delay(2000);
     aggiornamento =false;
     if(!alarmt)
     {
     botaggiornamento="";
     }
    }
  checkTelegram();
  }
   if ((clock() - timeStart) / CLOCKS_PER_SEC >= 1800)
  {
  autorizzazione = false;
  }
  if ((clock() - timeStart1) / CLOCKS_PER_SEC >= 40)
  {
  aggiornamento = true;
  }
  delay(100);
  
}

void checklora()
{
   message = lora.checkanswer();
  packetsize = message.length();
  if(packetsize!=0)
  {

    Serial.print("message in check");
    Serial.println(message);
    if(lora.getseriale(message)=="7101")
    {
      upload(lora.getseriale(message), message);
      int nt;
      while(nt<3)
      {
        lora.tx_pack("OK");
        delay(1000);
        nt++;
      } 
    }
    else
    {
    botaggiornamento=lora.getserial(0, message);
    Serial.println(botaggiornamento);
    String s =lora.getserial(4, message);
    
    if(s[0]=='1')
    {
      botaggiornamento += " aperto ";
      statocancello = "aperto";
    }
    if(s[2]=='1')
    {
      botaggiornamento += " autorizzato da codice ";
      alarmt= true;
    }else if(s[0]=='0'){statocancello = "chiuso"; botaggiornamento+= " chiuso"; botaggiornamento+= " batteria: " + lora.getserial(2, message)+ "V"; lora.tx_pack("OK");}
    
    else{
         botaggiornamento += "ingresso non autorizzato /autorizza";  
         autorizzazione=true;  
         lora.tx_pack("OK");     
      }
       Serial.println(botaggiornamento);
    }
  }
}



    




void checkTelegram()
{
  if (millis() - bot_lasttime > BOT_MTBS)
  {
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

    while (numNewMessages)
    {
      Serial.println("got response");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }

    bot_lasttime = millis();
  }

}

void handleNewMessages(int numNewMessages)
{
  Serial.println("handleNewMessages");
  Serial.println(String(numNewMessages));
  String keyboardJson = "[[\"/autorizza\"]]";
  for (int i = 0; i < numNewMessages; i++)
  {
    String chat_id = bot.messages[i].chat_id;
    String text = bot.messages[i].text;
    String from_name = bot.messages[i].from_name;
    if (from_name == "")
      from_name = "Guest";

    if (text == "accendi allarme" || text == "/accendi allarme" || text == "Accendi allarme")
    {
      bot.sendChatAction(chat_id, "typing");
      delay(400);
      bot.sendMessage(chat_id, "allarme acceso");
    }
    if (text == "autorizza" || text == "/autorizza" || text == "Autorizza")
    {
      autorizzazione=true;
      timeStart = clock();
      String keyboardinline = "[{ \"disattiva allarme\" : \"autorizza\",}]";
      bot.sendMessage(chat_id,"grazie "+ from_name + " ingresso autorizzato, ora puoi entrare"+ ".\n" );
      bot.sendMessageWithReplyKeyboard(chat_id, "", "", keyboardinline, true);
      idaut = String(chat_id);
      alarmt = false;
      bot.sendMessage(CHAT_ID, "autorizzato ingresso da " + from_name + ".\n");
      
    }
        if (text == "stato" || text == "/stato"|| text == "Stato")
    {
      bot.sendChatAction(chat_id, "typing");
      delay(400);
      bot.sendMessage(chat_id, "allarme acceso");
      bot.sendMessage(chat_id, "cancello" + statocancello);
    }


    if (text == "/start")
    {
      String welcome = "Welcome to il bot per tenere al sicuro la campagna " + from_name + ".\n";
     
      bot.sendMessageWithInlineKeyboard(chat_id, welcome, "", keyboardJson);
      tele = true;
    }
    if (text == "/stop")
    {
      tele = false;
      bot.sendMessage(chat_id, "aggiornamenti off");
    }
    
  }
}


void upload(String pathstring, String message)
{
  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
    String temp = "";
   HTTPClient http;   
   String url = server;
   Serial.println(pathstring);
   url += "?path=" + pathstring;
   Serial.println(url);
   Serial.println();
   http.begin(url);  //Specify destination for HTTP request
   http.addHeader("Content-Type", "text/plain");             //Specify content-type header
   
   int httpResponseCode;
    timerWrite(timer, 0); //reset timer (feed watchdog)   
    temp = message;
    temp += '\n';
    Serial.print(".");
    httpResponseCode = http.POST(temp);
    //do some action here
    Serial.print("file trasmitted ");
  
      //Send the actual POST request
  
   if(httpResponseCode>0){
  
    String response = http.getString();                       //Get the response to the request
  
    Serial.print("code: ");
    Serial.println(httpResponseCode);   //Print return code
   http.end();  //Free resources
  
  
 }else{
  
    Serial.println("Error in WiFi connection");   
  
 }
  }
  
}
