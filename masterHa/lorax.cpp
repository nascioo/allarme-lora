#include <Wire.h>
#include "lorax.h"
#include <StringSplitter.h>
#include <Preferences.h>
Preferences preferences;
String rssi = "RSSI --";
String packSize = "--";
String packet ;
#define NUM_RETRY_RADIO 3
#define DELAY_RISP_TX 5
unsigned long milli_s_prec;
unsigned long milli_s;
uint8_t time_read_data;
uint8_t num_pack = 0;
int rssi_rx = 0;
uint8_t retry_tx_radio = 0;
uint8_t delay_risp_tx = 0;
int count_crash = 0;
String mex_send, mex_rx;
#define INTERVALLO_CAMPIONI 900
#define LORA_CRASH INTERVALLO_CAMPIONI+100


void lorax::lorasend(uint16_t radice, uint16_t serial_num, double v_batt, String pkt)
{ 
  //caratteri per compatibilità geomilla 5
        uint8_t leng_memo = 100; 
        byte data[leng_memo];
        data[0]=0xD9;
        data[1]=0xD8;
        data[2]=0xFF;
        
  preferences.begin("radio_tx_data",false);
  rssi_rx = preferences.getUInt("rssi",0);
  num_pack = preferences.getUInt("num_pac",0);
    preferences.end();

  
  String packet_tx_string;
      packet_tx_string+=char(data[2]);
      packet_tx_string+=char(data[0]);
      packet_tx_string+=char(data[2]);
      packet_tx_string+=char(data[2]);
      packet_tx_string+=char(data[0]);
      packet_tx_string+=char(data[1]);
      packet_tx_string+=char(data[2]);
      packet_tx_string += (String(serial_num+radice) + ','+ String(num_pack) + ','+ String(v_batt,3)+","+ pkt +","+ String(rssi_rx));
      packet_tx_string+= char(data[1]);
      packet_tx_string+=char(data[2]);
  

  LoRa.setSignalBandwidth(500E3);
  LoRa.setSpreadingFactor(12);
  LoRa.setCodingRate4(8);
  // send packet
  LoRa.beginPacket(false);
  LoRa.setTxPower(20,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.print(packet_tx_string);
  LoRa.setPreambleLength(8);

  LoRa.endPacket();
}

//---------------------------FUNZIONI UTILIZZATE SEMPRE DA GEOMILLA 6------------------------------
void lorax::tx_pack(String memo_mex){
  //caratteri per compatibilità geomilla 5
        uint8_t leng_memo = 100; 
        byte data[leng_memo];
        data[0]=0xD9;
        data[1]=0xD8;
        data[2]=0xFF;


  preferences.begin("radio_tx_data",false);
  rssi_rx = preferences.getUInt("rssi",0);
  num_pack = preferences.getUInt("num_pac",0);
    preferences.end();

  Serial.println("message:'"+memo_mex+"'");
  String packet_tx_string;
      packet_tx_string+=char(data[2]);
      packet_tx_string+=char(data[0]);
      packet_tx_string+=char(data[2]);
      packet_tx_string+=char(data[2]);
      packet_tx_string+=char(data[0]);
      packet_tx_string+=char(data[1]);
      packet_tx_string+=char(data[2]);
      packet_tx_string += (memo_mex);
      packet_tx_string+= char(data[2]);
      packet_tx_string+=char(data[1]);


  LoRa.setSignalBandwidth(500E3);
  LoRa.setSpreadingFactor(12);
  LoRa.setCodingRate4(8);
  // send packet
  LoRa.beginPacket(false);
  LoRa.setTxPower(20,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.print(packet_tx_string);
  LoRa.setPreambleLength(8);

  LoRa.endPacket();
}


void lorax::orologio(void){
  milli_s = abs(millis() - milli_s_prec);
  if(milli_s>999){ //secondi
    milli_s_prec = millis();
    milli_s = 0;
    time_read_data++;
    delay_risp_tx++;
  }
  return;
}




String lorax::checkanswer(){
 delay_risp_tx = 0;
      int packetsize = 0;
      Serial.print("test");
      packetsize = LoRa.parsePacket();
      packet = read_lora_pac(packetsize); 
      if(packetsize!=0)
      {   
      tx_pack("3001,9,"+packet.substring(0,4));
      }
      return packet;
}


//ricevitore**************************************************************
String lorax::read_lora_pac(int packetsize){
  String packet ="" ;
  if (packetsize == 0){
    return packet;          // if there's no packet, return
  }
  // read packet header bytes:
  Serial.println("ricevuto qualcosa");
  String memos = "";
  while (LoRa.available())
  {
    memos += (char)LoRa.read();
  }

  rssi_rx =Heltec.LoRa.packetRssi();
  
  return memos.substring(7,memos.length()-2)+","+ rssi_rx;
}


String lorax::getserial(String pp){
 
 StringSplitter *splitter = new StringSplitter(pp, ',', 30);
pp = "";
pp += splitter->getItemAtIndex(0);
return pp;
}
