#include "heltec.h"
#include <StringSplitter.h>
#include <WiFi.h>
#include "lorax.h"
#include "esp_system.h"
#include <Wire.h>
#include <PubSubClient.h>
#define MQTT_KEEPALIVE 60
lorax lora;
#define wifi_ssid "elmarx"
#define wifi_password "Elmarx4Me"
#define BAND 868E6       //you can set band here directly,e.g. 868E6,915E6
#define mqtt_server "192.168.77.171"
#define mqtt_user "mqtt_user"
#define mqtt_password "Elmarxsrl"
RTC_DATA_ATTR int bootCount = 0;
long start;
bool connected = false;
const int wdtTimeout = 1820000; 
hw_timer_t *timer = NULL;
String message ;
int packetsize;
char topic[7];
int n_cam=10;
char charBuf[50];
double now=0;
//---------------------------watchdog initialize----------------------------------
void IRAM_ATTR resetModule() {
  ets_printf("wdreboot\n");
  esp_restart();
}


WiFiClient espClient;
PubSubClient client(espClient); 

void setup() {
   Heltec.begin(false /*DisplayEnable Enable*/, true /*LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
   delay(100);
     timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);     
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  start =  millis();
  // Set SDA and SDL ports
  Wire.begin(2, 14);


}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(1500);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
     
    if (client.connect("ESP32Client", mqtt_user, mqtt_password)) {
      Serial.println("connected");
      connected = true;
    } else {
      connected = false;
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


double lastMsg = 0;
float temp = 0.0;
float hum = 0.0;
float diff = 1.0;

void loop() {
  now = millis();
   if (now - start > 2700000) 
   {
     ESP.restart();
   }
  if (!client.connected()) {
    reconnect();
  }

    if (!client.loop()) {
    Serial.print("Client disconnected...");
    if (client.connect("arduinoPublisher")) {
      Serial.println("reconnected.");
    } else {
      Serial.println("failed.");
    }
  } 
  

    message = lora.checkanswer();
    
    packetsize = message.length();

    if (packetsize!=0) {
      timerWrite(timer, 0);
      Serial.print("ricevuto pacchetto");
        message.substring(0,message.length()).toCharArray(charBuf, 50);
        char *token;
        token= strtok(charBuf, ",");
        String tempser = String(token);
        token = strtok(NULL, ","); 
        if(tempser.toInt()<10000)
        {
        for(int i=1; i<=n_cam; i++)
        {
        String serstr= tempser+"/"+i; 
        serstr.toCharArray(topic, 7);
        chttostr(String(token), topic);
        token = strtok(NULL, ","); 
        }
        }
        packetsize=0;
        message="";
    }
        if (now - lastMsg > 100000) {
      lastMsg = now; 
      char msg[2] = {'k', 'k'};
      client.publish("3001", msg);
       msg[0] ='o'; 
      Serial.println("sendo ok");
      client.publish("3001", msg);
      LoRa.setFrequency(BAND);
        LoRa.setSignalBandwidth(500E3);
  LoRa.setSpreadingFactor(12);
  LoRa.setCodingRate4(8);
  // send packet
  LoRa.beginPacket(false);
  LoRa.setTxPower(20,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.setPreambleLength(8);
    }
  delay(100);
}

void chttostr(String str, char topic[7])
{

  int str_len = str.length() + 1; 

// Prepare the character array (the buffer) 
  char char_array[str_len];

// Copy it over 
    texttopage(str).toCharArray(char_array, str_len);
    
    Serial.print(String(topic));
    Serial.println(String(char_array));
    client.publish(topic, char_array);
}

String texttopage(String st)
{
  byte data[100];
data[0]=0x7E;
data[1]=0xD8;
data[2]=0xFF;

  for(int j=0; j<= st.length(); j++)
  {
    if(st[j]<=data[0])
    {
      
    }
    else
    {
      st[j]='?';
    }
  }
  return st;
}