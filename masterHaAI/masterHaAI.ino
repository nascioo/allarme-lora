#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <SPI.h>
#include <heltec.h>

#define BAND 868E6       //you can set band here directly,e.g. 868E6,915E6
const char* ssid = "elmarx";
const char* password = "Elmarx4Me";
const char* mqttServer = "192.168.77.171";
const int mqttPort = 1883;
const char* mqttUser = "mqtt_user";
const char* mqttPassword = "Elmarxsrl";

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
   Heltec.begin(false /*DisplayEnable Enable*/, true /*LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
        LoRa.setFrequency(BAND);
        LoRa.setSignalBandwidth(500E3);
  LoRa.setSpreadingFactor(12);
  LoRa.setCodingRate4(8);
  // send packet
  LoRa.beginPacket(false);
  LoRa.setTxPower(20,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.setPreambleLength(8);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println("Connected to WiFi");
 

  client.setServer(mqttServer, mqttPort);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    String message = "";
    while (LoRa.available()) {
      message += (char)LoRa.read();
    }

    int commaIndex = message.indexOf(',');
    int lastCommaIndex = 0;
    int elementIndex = 1;
    String topic = "";
    String payload = "";
    while (commaIndex >= 0) {
      topic = "lora/element" + String(elementIndex);
      payload = message.substring(lastCommaIndex, commaIndex);
      client.publish(topic.c_str(), payload.c_str());
      lastCommaIndex = commaIndex + 1;
      commaIndex = message.indexOf(',', lastCommaIndex);
      elementIndex++;
    }
    topic = "lora/element" + String(elementIndex);
    payload = message.substring(lastCommaIndex);
    client.publish(topic.c_str(), payload.c_str());

    // Respond with the first element
    commaIndex = message.indexOf(',');
    if (commaIndex >= 0) {
      String firstElement = message.substring(0, commaIndex);
       LoRa.setFrequency(BAND);
        LoRa.setSignalBandwidth(500E3);
  LoRa.setSpreadingFactor(12);
  LoRa.setCodingRate4(8);
  // send packet
  LoRa.beginPacket(false);
  LoRa.setTxPower(20,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.setPreambleLength(8);
      LoRa.beginPacket();
      LoRa.print(firstElement);
      LoRa.endPacket();
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("Heltec_Receiver", mqttUser, mqttPassword)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}